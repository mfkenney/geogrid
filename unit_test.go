package geogrid

import "testing"

func TestProjection(t *testing.T) {
	lat := 73.126597
	lon := -149.420463

	p, err := NewProjection(lon, lat)
	if err != nil {
		t.Fatal(err)
	}

	gpts, err := p.Forward([]float64{lon, lat})
	if err != nil {
		t.Fatal(err)
	}

	if gpts[0] != 0 || gpts[1] != 0 {
		t.Errorf("Bad transformation; expected [0, 0], got %v", gpts)
	}
}

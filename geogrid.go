// Package geogrid transforms between geodetic (lat-lon) coordinates and
// X-Y grid coordinates.
package geogrid

import (
	"errors"
	"fmt"

	"github.com/go-spatial/proj/core"
	_ "github.com/go-spatial/proj/operations"
	"github.com/go-spatial/proj/support"
)

var ErrInputLen = errors.New("An even number of input values is required")

// Projection represents a map grid projection
type Projection struct {
	system    *core.System
	op        core.IOperation
	converter core.IConvertLPToXY
	origin    core.CoordXY
}

// NewProjection returns a new Mercator projection centered at lon, lat
func NewProjection(lon, lat float64) (*Projection, error) {
	ps, err := support.NewProjString(
		fmt.Sprintf("+proj=merc +lon_0=%.6f +lat_ts=%.6f +datum=WGS84",
			lon, lat))

	sys, opx, err := core.NewSystem(ps)
	if err != nil {
		return nil, err
	}

	if !opx.GetDescription().IsConvertLPToXY() {
		return nil, fmt.Errorf("projection type is not supported")
	}

	p := &Projection{
		system:    sys,
		op:        opx,
		converter: opx.(core.IConvertLPToXY),
	}
	xy, _ := p.Forward([]float64{lon, lat})
	p.origin.X = xy[0]
	p.origin.Y = xy[1]

	return p, nil
}

// Forward converts an array of lon-lat values to X-Y. Longitude values
// (degrees) must be stored in the even array indicies and latitude values
// in degrees. The returned X-Y values (meters) are stored similarly.
func (p *Projection) Forward(input []float64) ([]float64, error) {
	if len(input)%2 != 0 {
		return nil, ErrInputLen
	}

	output := make([]float64, len(input))

	lp := &core.CoordLP{}
	for i := 0; i < len(input); i += 2 {
		lp.Lam = support.DDToR(input[i])
		lp.Phi = support.DDToR(input[i+1])

		xy, err := p.converter.Forward(lp)
		if err != nil {
			return nil, err
		}

		output[i] = xy.X - p.origin.X
		output[i+1] = xy.Y - p.origin.Y
	}

	return output, nil
}

// Inverse converts an array of X-Y values to lon-lat. This method uses the same
// storage conventions as Forward.
func (p *Projection) Inverse(input []float64) ([]float64, error) {
	if len(input)%2 != 0 {
		return nil, ErrInputLen
	}

	output := make([]float64, len(input))

	xy := &core.CoordXY{}

	for i := 0; i < len(input); i += 2 {
		xy.X = input[i] + p.origin.X
		xy.Y = input[i+1] + p.origin.Y

		lp, err := p.converter.Inverse(xy)

		if err != nil {
			return nil, err
		}

		l, p := lp.Lam, lp.Phi

		output[i] = support.RToDD(l)
		output[i+1] = support.RToDD(p)
	}

	return output, nil
}

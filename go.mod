module bitbucket.org/mfkenney/geogrid

go 1.13

require (
	github.com/go-spatial/proj v0.2.0
	github.com/stretchr/testify v1.6.1 // indirect
)
